# ====== Importar Maesages Framework ======
from django.contrib import messages
from django.contrib.auth import authenticate, logout, login as auth_login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import redirect, render
from rest_framework import status, viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response

# ====== Importar Modelos ======
from .models import Perfil, Pregunta, Encuesta, UsuarioPersonaEncuestada, EncuestaRealizada, Respuesta
from .serializers import PerfilSerializer, UsuarioSerializer, EncuestaSerializer, PreguntaSerializer, UsuarioPersonaEncuestadaSerializer, EncuestaRealizadaSerializer, RespuestaSerializer


# ====== ViewSets ======
class UsuarioViewSet(viewsets.ModelViewSet):
    queryset = User.objects.select_related('perfil')
    serializer_class = UsuarioSerializer


class PerfilViewSet(viewsets.ModelViewSet):
    queryset = Perfil.objects.select_related()
    serializer_class = PerfilSerializer


class EncuestaViewSet(viewsets.ModelViewSet):
    queryset = Encuesta.objects.select_related()
    serializer_class = EncuestaSerializer


class PreguntaViewSet(viewsets.ModelViewSet):
    queryset = Pregunta.objects.all()
    serializer_class = PreguntaSerializer


class UsuarioPersonaEncuestadaViewSet(viewsets.ModelViewSet):
    queryset = UsuarioPersonaEncuestada.objects.all()
    serializer_class = UsuarioPersonaEncuestadaSerializer


class EncuestaRealizadaViewSet(viewsets.ModelViewSet):
    queryset = EncuestaRealizada.objects.all()
    serializer_class = EncuestaRealizadaSerializer


class RespuestaViewSet(viewsets.ModelViewSet):
    queryset = Respuesta.objects.all()
    serializer_class = RespuestaSerializer

# <===== Vistas Template =====>


def login(request):
    return render(request, 'login.html', {})


@login_required(login_url='login')
def index(request):
    if request.user.is_superuser:
        return render(request, 'index.html', {'perfil_usuario': request.user})
    else:
        perfil_usuario = Perfil.objects.get(usuario=request.user)
        if(perfil_usuario.descripcion_perfil == "Encuestador"):
            return render(request, 'realizar_encuesta.html', {'perfil_usuario': Perfil.objects.get(usuario=request.user), 'lista_encuestas': Encuesta.objects.select_related()})
        else:
            return render(request, 'index.html', {'perfil_usuario': Perfil.objects.get(usuario=request.user)})


@login_required(login_url='login')
def encuestas(request):
    if request.user.is_superuser:
        return render(request, 'encuesta.html', {'perfil_usuario': request.user, 'encuestas': Encuesta.objects.all()})
    else:
        return render(request, 'encuesta.html',
                      {'perfil_usuario': Perfil.objects.get(usuario=request.user), 'encuestas': Encuesta.objects.all()})


@login_required(login_url='login')
def preguntas(request):
    if request.user.is_superuser:
        return render(request, 'preguntas.html',
                      {'perfil_usuario': request.user, 'lista_preguntas': Pregunta.objects.all(), 'lista_encuestas': Encuesta.objects.select_related()})
    else:
        return render(request, 'preguntas.html', {'perfil_usuario': Perfil.objects.get(usuario=request.user),
                                                  'lista_preguntas': Pregunta.objects.all(), 'lista_encuestas': Encuesta.objects.select_related()})


@login_required(login_url='login')
def realizar_encuesta(request):
    return render(request, 'realizar_encuesta.html', {'perfil_usuario': Perfil.objects.get(usuario=request.user), 'lista_encuestas': Encuesta.objects.select_related()})


@login_required(login_url='login')
def responder_encuesta(request):
    input_encuesta = request.POST.get('input_encuesta', '')
    encuesta_seleccionada = Encuesta.objects.select_related().get(id=input_encuesta)
    lista_preguntas = Pregunta.objects.filter(encuesta__pk=input_encuesta)

    return render(request, 'responder_encuesta.html', {'perfil_usuario': Perfil.objects.get(usuario=request.user), 'encuesta_seleccionada': encuesta_seleccionada, 'lista_preguntas': lista_preguntas})


@login_required(login_url='login')
def encuestas_realizadas(request):
    if request.user.is_superuser:
        return render(request, 'encuestas_realizadas.html', {'perfil_usuario': request.user, 'lista_encuestas_realizadas': EncuestaRealizada.objects.select_related()})
    else:
        return render(request, 'encuestas_realizadas.html', {'perfil_usuario': Perfil.objects.get(usuario=request.user), 'lista_encuestas_realizadas': EncuestaRealizada.objects.select_related()})


# <===== LogIn / LogOut =====>
def logearse(request):
    nombre = request.POST.get("usuario", '')
    contrasenia = request.POST.get("contrasenia", '')
    user = authenticate(request, username=nombre, password=contrasenia)

    if user is not None:
        auth_login(request, user)
        request.session['usuario'] = user.username
        return redirect("index")
    else:
        messages.error(request, 'El usuario o la contraseña no es válido ')
        return redirect("login")


@login_required(login_url='login')
def cerrar_sesion(request):
    logout(request)
    return redirect("login")


# ====== CRUD USUARIO ======
def crear_usuario(request):
    nombre_input = request.POST.get('nombre', '')
    apellido_input = request.POST.get('apellido', '')
    email_input = request.POST.get('email', '')
    contrasenia_input = request.POST.get('contrasenia', '')
    perfil_input = request.POST.get('perfil', '')

    perfil_usuario = Perfil()
    perfil_usuario.descripcion_perfil = perfil_input
    perfil_usuario.usuario = User.objects.create_user(username=email_input, first_name=nombre_input,
                                                      last_name=apellido_input, email=email_input,
                                                      password=contrasenia_input)
    perfil_usuario.save()

    messages.success(request, 'Usuario "' + email_input + '" ha sido AGREGADO')
    return redirect("index")


def modificar_usuario(request):
    id_input = request.POST.get('id_usuario', '')
    nombre_modificar = request.POST.get('nombre_modificar', '')
    apellido_modificar = request.POST.get('apellido_modificar', '')
    email_modificar = request.POST.get('email_modificar', '')
    perfil_modificar = request.POST.get('perfil', '')

    perfil_usuario = Perfil.objects.get(pk=id_input)
    perfil_usuario.usuario.first_name = nombre_modificar
    perfil_usuario.usuario.last_name = apellido_modificar
    perfil_usuario.usuario.email = email_modificar
    perfil_usuario.descripcion_perfil = perfil_modificar
    perfil_usuario.usuario.save()
    perfil_usuario.save()

    messages.success(request, 'Usuario "' +
                     perfil_usuario.usuario.email + '" ha sido MODIFICADO')
    return redirect("index")


def eliminar_usuario(request, id):
    perfil_usuario = Perfil.objects.get(pk=id)
    perfil_usuario.usuario.delete()

    messages.success(request, 'Usuario "' +
                     perfil_usuario.usuario.username + '" ha sido ELIMINADO')
    return redirect("index")


# ====== ENCUESTA ======
def crear_encuesta(request):
    titulo_encuesta = request.POST.get('titulo_encuesta', '')

    encuesta = Encuesta(titulo_encuesta=titulo_encuesta)
    encuesta.save()

    messages.success(request, 'Encuesta ha sido CREADA')

    return redirect("encuestas")


def modificar_encuesta(request):
    id_input = request.POST.get('id_encuesta', '')
    nombre_modificar = request.POST.get('nombre_modificar', '')

    encuesta = Encuesta.objects.get(pk=id_input)
    encuesta.titulo_encuesta = nombre_modificar
    encuesta.save()
    messages.success(request, 'Encuesta "' +
                     encuesta.titulo_encuesta + '" ha sido MODIFICADA')
    return redirect("encuestas")


def eliminar_encuesta(request, id):
    encuesta = Encuesta.objects.get(pk=id)
    encuesta.delete()
    messages.success(request, 'Encuesta ha sido ELIMINADA')
    return redirect("encuestas")


# ====== PREGUNTAS ======
def crear_pregunta(request):
    pregunta_post = request.POST.get('input_agregar_pregunta', '')
    encuesta_post = request.POST.get('input_pregunta_encuesta', '')

    encuesta = Encuesta.objects.get(pk=encuesta_post)

    pregunta = Pregunta()
    pregunta.desc_pregunta = pregunta_post
    pregunta.encuesta = encuesta
    pregunta.save()
    messages.success(request, 'Pregunta ha sido CREADA')

    return redirect("preguntas")


def eliminar_pregunta(request, id):
    pregunta = Pregunta.objects.get(pk=id)
    pregunta.delete()
    messages.success(request, 'Pregunta ha sido ELIMINADA')
    return redirect("preguntas")


def modificar_pregunta(request):

    pregunta_id = request.POST.get("id_pregunta", '')
    pregunta = Pregunta.objects.get(pk=pregunta_id)
    pregunta_modificar = request.POST.get('pregunta_modificar', '')
    messages.success(request, 'Pregunta"' +
                     pregunta.desc_pregunta + '" ha sido MODIFICADA')
    pregunta.desc_pregunta = pregunta_modificar
    pregunta.save()
    return redirect("preguntas")


# ====== RESPUESTAS ======
def guardar_respuestas(request):
    # Obtener datos POST
    rut = request.POST.get("input_rut", '')
    nombre_completo = request.POST.get("input_nombre_completo", '')
    ciudad = request.POST.get("input_ciudad", '')
    direccion = request.POST.get("input_direccion", '')
    email = request.POST.get("email_modificar", '')

    # Crear Encuestado
    encuestado = UsuarioPersonaEncuestada()
    encuestado.rut = rut
    encuestado.nombre = nombre_completo
    encuestado.direccion = direccion
    encuestado.ciudad = ciudad
    encuestado.correo = email
    encuestado.save()

    # Buscar Encuesta
    id_encuesta = request.POST.get("input_id_encuesta", '')
    encuesta = Encuesta.objects.get(id=id_encuesta)

    # Se crea una encuesta entre el usuario y el encuestado
    nueva_encuesta = EncuestaRealizada()
    nueva_encuesta.encuesta = encuesta
    usuario_encuestador = Perfil.objects.get(usuario=request.user)
    nueva_encuesta.fk_encuestador = usuario_encuestador.usuario
    nueva_encuesta.fk_encuestado = encuestado
    nueva_encuesta.save()

    # Obtener respuestas POST
    for key, value in request.POST.items():
        if key.startswith('pregunta'):
            pregunta = Pregunta.objects.get(id=value)
            print(key)
            print(value)

        if key.startswith('respuesta'):
            respuesta = Respuesta()
            respuesta.respuesta = value
            respuesta.fk_pregunta = pregunta
            respuesta.fk_encuesta_realizada = nueva_encuesta
            respuesta.save()
            print(key)
            print(value)

    messages.success(request, 'Encuesta ha sido creada correctamente')

    return redirect("realizar_encuesta")
