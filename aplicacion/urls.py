from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'usuarios', views.UsuarioViewSet)
router.register(r'perfiles_usuarios', views.PerfilViewSet)
router.register(r'encuestas', views.EncuestaViewSet)
router.register(r'preguntas', views.PreguntaViewSet)
router.register(r'encuestados', views.UsuarioPersonaEncuestadaViewSet)
router.register(r'encuestas_realizadas', views.EncuestaRealizadaViewSet)
router.register(r'respuestas', views.RespuestaViewSet)

urlpatterns = [
    # Index
    path('', views.index, name="index"),
    # Login
    path('Login', views.login, name="login"),
    path('login/logearse', views.logearse, name="logearse"),
    path('cerrar_sesion', views.cerrar_sesion,
         name="cerrar_sesion"),
    # CRUD Usuario
    path('crear_usuario', views.crear_usuario,
         name="crear_usuario"),
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls',
                              namespace='rest_framework')),
    path('usuarios/eliminar/<int:id>',
         views.eliminar_usuario, name="eliminar_usuario"),
    path('usuarios/modificar',
         views.modificar_usuario, name="modificar_usuario"),
    # CRUD Encuestas
    path('encuestas', views.encuestas, name="encuestas"),
    path('crear_encuesta', views.crear_encuesta,
         name="crear_encuesta"),
    path('encuestas/eliminar/<int:id>', views.eliminar_encuesta,
         name="eliminar_encuesta"), path('preguntas', views.preguntas, name="preguntas"),
    path('encuestas/modificar',
         views.modificar_encuesta, name="modificar_encuesta"),
    path('encuesta/realizar_encuesta',
         views.realizar_encuesta, name="realizar_encuesta"),
    path('encuesta/responder_encuesta',
         views.responder_encuesta, name="responder_encuesta"),
    path('encuesta/guardar_respuestas',
         views.guardar_respuestas, name="guardar_respuestas"),
     path('encuesta/encuestas_realizadas',
         views.encuestas_realizadas, name="encuestas_realizadas"),
    # CRUD Preguntas
    path('crear_pregunta', views.crear_pregunta,
         name="crear_pregunta"),
    path('preguntas/eliminar/<int:id>',
         views.eliminar_pregunta, name="eliminar_pregunta"),
    path('preguntas/modificar/>',
         views.modificar_pregunta, name="modificar_pregunta"),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
