$(document).ready(function () {
    $("#form_login").validate({
        rules: {
            usuario: {
                required: true,
                maxlength: 100,
            },
            contrasenia: {
                required: true,
                maxlength: 100,
            },
        },
        messages: {
            usuario: {
                required: "Campo requerido",
                maxlength: "No se pueden ingresar más de 100 caracteres",
            },
            contrasenia: {
                required: "Campo requerido",
                maxlength: "No se pueden ingresar más de 100 caracteres",
            },
        }
    });
});