$(document).ready(function () {
    $("#form_agregausuario").validate({
        rules: {
            nombre: {
                required: true,
                maxlength: 100,
            },
            apellido: {
                required: true,
                maxlength: 100,
            },
            email: {
                required: true,
                maxlength: 100,
                email: true
            },
            contrasenia: {
                required: true,
                maxlength: 100,
            },
            perfil: {
                required: true,
            }
        },
        messages: {
            nombre: {
                required: "Campo requerido",
                maxlength: "No se pueden ingresar más de 100 caracteres",
            },
            apellido: {
                required: "Campo requerido",
                maxlength: "No se pueden ingresar más de 100 caracteres",
            },
            email: {
                required: "Campo requerido",
                maxlength: "No se pueden ingresar más de 100 caracteres",
                email: "Formato de correo electronico incorrecto"
            },
            contrasenia: {
                required: "Campo requerido",
                maxlength: "No se pueden ingresar más de 100 caracteres",
            },
            perfil: {
                required: "Campo requerido",
            }
        }
    });
});