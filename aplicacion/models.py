from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class Perfil(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    descripcion_perfil = models.CharField(max_length=30)

    class Meta:
        verbose_name = 'Perfil'
        verbose_name_plural = 'Perfiles'
        ordering = ['usuario']

    def __str__(self):
        return self.descripcion_perfil


class Encuesta(models.Model):
    titulo_encuesta = models.CharField(max_length=40)

    def __str__(self):
        return "Encuesta"


class Pregunta(models.Model):
    desc_pregunta = models.CharField(max_length=255)
    encuesta = models.ForeignKey(
        Encuesta, related_name='preguntas', on_delete=models.CASCADE)

    def __str__(self):
        return "Pregunta"


class UsuarioPersonaEncuestada(models.Model):
    rut = models.CharField(max_length=20)
    nombre = models.CharField(max_length=100)
    direccion = models.CharField(max_length=100)
    ciudad = models.CharField(max_length=100)
    correo = models.CharField(max_length=100)

    def __str__(self):
        return "Persona a Encuestar"


class EncuestaRealizada(models.Model):
    encuesta = models.OneToOneField(Encuesta, on_delete=models.CASCADE)
    fk_encuestador = models.ForeignKey(User, on_delete=models.CASCADE)
    fk_encuestado = models.ForeignKey(
        UsuarioPersonaEncuestada, on_delete=models.CASCADE)

    def __str__(self):
        return "Encuesta Realizada"


class Respuesta(models.Model):
    respuesta = models.CharField(max_length=255)
    fk_pregunta = models.ForeignKey(Pregunta, on_delete=models.CASCADE)
    fk_encuesta_realizada = models.ForeignKey(
        EncuestaRealizada, on_delete=models.CASCADE)

    def __str__(self):
        return "Pregunta"
