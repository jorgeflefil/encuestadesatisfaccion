from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Perfil, Encuesta, Pregunta, UsuarioPersonaEncuestada, EncuestaRealizada, Respuesta


class UsuarioSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    class Meta:
        model = User
        fields = ('url', 'id', 'first_name',
                  'last_name', 'email')


class PerfilSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    usuario = UsuarioSerializer()

    class Meta:
        model = Perfil
        # fields = ('url', 'pk', 'usuario', 'descripcion_perfil')
        fields = ('id', 'descripcion_perfil', 'usuario')
        datatables_always_serialize = ('id',)


class PreguntaSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Pregunta
        fields = ('url', 'id', 'desc_pregunta')
        datatables_always_serialize = ('id',)


class EncuestaSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    preguntas = PreguntaSerializer(many=True, read_only=True)

    class Meta:
        model = Encuesta
        fields = ('url', 'id', 'titulo_encuesta', 'preguntas')
        datatables_always_serialize = ('id',)


class UsuarioPersonaEncuestadaSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = UsuarioPersonaEncuestada
        fields = ('url', 'id', 'rut', 'nombre',
                  'direccion', 'ciudad', 'correo')
        datatables_always_serialize = ('id',)


class EncuestaRealizadaSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    encuesta = EncuestaSerializer(read_only=True)
    fk_encuestador = UsuarioSerializer(read_only=True)
    fk_encuestado = UsuarioPersonaEncuestadaSerializer(read_only=True)

    class Meta:
        model = EncuestaRealizada
        fields = ('url', 'id', 'encuesta', 'fk_encuestador', 'fk_encuestado')
        datatables_always_serialize = ('id',)


class RespuestaSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    pregunta = serializers.CharField(source='fk_pregunta.desc_pregunta')

    class Meta:
        model = Respuesta
        fields = ('url', 'id', 'pregunta',
                  'respuesta', 'fk_encuesta_realizada')
        datatables_always_serialize = ('id',)
